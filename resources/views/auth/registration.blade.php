@extends('layout')
  
@section('content')
<main class="login-form">
  <div class="cotainer">
      <div class="row justify-content-center">
          <div class="col-md-8">
              <div class="card">
                  <div class="card-header">Register</div>
                  <div class="card-body">
  
                      <form action="{{ route('register.post') }}" method="POST">
                          @csrf
                         
                          <div class="form-group row">
                              <label for="firstname" class="col-md-4 col-form-label text-md-right">First Name</label>
                              <div class="col-md-6">
                                  <input type="text" id="firstname" class="form-control" name="firstname" required autofocus>
                                  @if ($errors->has('firstname'))
                                      <span class="text-danger">{{ $errors->first('firstname') }}</span>
                                  @endif
                              </div>
                          </div>

                          <div class="form-group row">
                              <label for="lastname" class="col-md-4 col-form-label text-md-right"> Last Name</label>
                              <div class="col-md-6">
                                  <input type="text" id="lastname" class="form-control" name="lastname" required autofocus>
                                  @if ($errors->has('lastname'))
                                      <span class="text-danger">{{ $errors->first('lastname') }}</span>
                                  @endif
                              </div>
                          </div>
  
                          <div class="form-group row">
                              <label for="email_address" class="col-md-4 col-form-label text-md-right">E-Mail Address</label>
                              <div class="col-md-6">
                                  <input type="text" id="email_address" class="form-control" name="email" required autofocus>
                                  @if ($errors->has('email'))
                                      <span class="text-danger">{{ $errors->first('email') }}</span>
                                  @endif
                              </div>
                          </div>
  
                          <div class="form-group row">
                              <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>
                              <div class="col-md-6">
                                  <input type="password" id="password" class="form-control" name="password" required>
                                  @if ($errors->has('password'))
                                      <span class="text-danger">{{ $errors->first('password') }}</span>
                                  @endif
                              </div>
                          </div>
                          <div class="form-group row">
                              <label for="password" class="col-md-4 col-form-label text-md-right">Confirm Password</label>
                              <div class="col-md-6">
                                  <input type="password" id="password" class="form-control" name="password" required>
                                  @if ($errors->has('password'))
                                      <span class="text-danger">{{ $errors->first('password') }}</span>
                                  @endif
                              </div>
                          </div>

                          <div class="form-group row">
                                <label for="date" class="col-md-4 col-form-label text-md-right">Date Of Birth</label>
                                <div class="col-md-6">
                                    <input type="date" id="date" class="form-control" name="date" required>
                                    @if ($errors->has('date'))
                                        <span class="text-danger">{{ $errors->first('date') }}</span>
                                    @endif
                                </div>
                          </div>

                          <div class="form-group row">
                              <label for="income" class="col-md-4 col-form-label text-md-right">Annual Income</label>
                              <div class="col-md-6">
                                  <input type="textarea" id="income" class="form-control" name="income" required>
                                  @if ($errors->has('income'))
                                      <span class="text-danger">{{ $errors->first('income') }}</span>
                                  @endif
                              </div>
                          </div>
                          <div class="form-group row">
                              <label for="gender" class="col-md-4 col-form-label text-md-right">Gender</label>
                              <div class="col-md-6">
                                  <input type="radio" id="female" value="female"  name="gender" required>
                                  <label for="female">Female</label>
                                  @if ($errors->has('female'))
                                      <span class="text-danger">{{ $errors->first('female') }}</span>
                                  @endif
                                  <input type="radio" id="male"  value="male"   name="gender" required>
                                  <label for="male">Male</label>
                                  @if ($errors->has('male'))
                                      <span class="text-danger">{{ $errors->first('male') }}</span>
                                  @endif
                              </div>
                          </div>
                          <div class="form-group row">
                              <label for="occupation" class="col-md-4 col-form-label text-md-right">Occupation</label>
                              <div class="col-md-6">
                              <select name="occupation" class="form-control" id="occupation">
                                    <option value="privatejob">Private Job</option>
                                    <option value="governmentjob">Government Job</option>
                                    <option value="business">Business</option>
                                </select>
                                  @if ($errors->has('income'))
                                      <span class="text-danger">{{ $errors->first('occupation') }}</span>
                                  @endif
                              </div>
                          </div>
                          <div class="form-group row">
                              <label for="familytype" class="col-md-4 col-form-label text-md-right">Family Type</label>
                              <div class="col-md-6">
                              <select name="familytype" class="form-control"  id="familytype">
                                    <option value="jointfamily">Joint Family</option>
                                    <option value="nuclearfamily">Nuclear Family</option>
                                </select>
                                  @if ($errors->has('income'))
                                      <span class="text-danger">{{ $errors->first('familytype') }}</span>
                                  @endif
                              </div>
                          </div>
                            
                          <div class="form-group row">
                              <label for="manglik" class="col-md-4 col-form-label text-md-right">Manglik</label>
                              <div class="col-md-6">
                              <select name="manglik" class="form-control"  id="manglik">
                                    <option value="yes">Yes</option>
                                    <option value="no">No</option>
                                </select>
                                  @if ($errors->has('income'))
                                      <span class="text-danger">{{ $errors->first('manglik') }}</span>
                                  @endif
                              </div>
                          </div>
                          <h4>Partner Preference</h4>

                            <div class="form-group row">
                                <label for="expectedincome" class="col-md-4 col-form-label text-md-right">Expected Income</label>
                                <div class="col-md-6">
                                    <div class="slidecontainer">
                                        <input type="range" name="expectedincome" min="100" max="5000000" value="0" class="slider" id="expectedincome">
                                        <p>Value: <span id="demo"></span></p>
                                    </div>

                                    @if($errors->has('expectedincome'))
                                        <span class="text-danger">{{ $errors->first('expectedincome') }}</span>
                                    @endif
                                </div>
                            </div>

                          <div class="form-group row">
                              <label for="occupation" class="col-md-4 col-form-label text-md-right">Occupation</label>
                              <div class="col-md-6">
                                <select name="partner_occupation" class="form-control"  id="occupation">
                                    <option value="privatejob">Private Job</option>
                                    <option value="governmentjob">Government Job</option>
                                    <option value="business">Business</option>
                                </select>
                                  @if ($errors->has('income'))
                                      <span class="text-danger">{{ $errors->first('occupation') }}</span>
                                  @endif
                              </div>
                          </div>
                          <div class="form-group row">
                              <label for="familytype" class="col-md-4 col-form-label text-md-right">Family Type</label>
                              <div class="col-md-6">
                                <select name="partner_familytype" class="form-control"  id="familytype">
                                    <option value="jointfamily">Joint Family</option>
                                    <option value="nuclearfamily">Nuclear Family</option>
                                </select>
                                  @if ($errors->has('income'))
                                      <span class="text-danger">{{ $errors->first('familytype') }}</span>
                                  @endif
                              </div>
                          </div>
                            
                          <div class="form-group row">
                              <label for="manglik" class="col-md-4 col-form-label text-md-right">Manglik</label>
                              <div class="col-md-6">
                                <select name="partner_manglik" id="manglik" class="form-control" >
                                    <option value="yes">Yes</option>
                                    <option value="no">No</option>
                                </select>
                                  @if ($errors->has('income'))
                                      <span class="text-danger">{{ $errors->first('manglik') }}</span>
                                  @endif
                              </div>
                          </div>

                         
  
                          <div class="col-md-6 offset-md-4">
                              <button type="submit" class="btn btn-primary">
                                  Register
                              </button>
                          </div>
                      </form>
                        
                  </div>
              </div>
          </div>
      </div>
  </div>
</main>

<script>
    var slider = document.getElementById("expectedincome");
    var output = document.getElementById("demo");
    output.innerHTML = slider.value;
    slider.oninput = function() {
        output.innerHTML = this.value;
    }
</script>
@endsection