<div class="form-group">
<label for="{{$name}}" class="">{{$label}} @if(($required ?? '')=='true')<span class="text-danger">*</span>@endif</label>
<input type="{{$type??'text'}}" maxlength="255" {{ $attributes }}  class="form-control @error($name) is-invalid @enderror"  name="{{$name}}"  value="{{old($name, $value ?? '')}}" @if(($required ?? '')=='true') required @endif />
@error($name)
    <x-error :message="$message"/>
@enderror
</div>

