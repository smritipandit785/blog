@extends('admin.index')

@section('content')
    <div class="py-12">
        <div class="card card-body">


            <table class="table" id='table'>
                <thead>
                    <th>#</th>
                    <th>First Name </th>
                    <th>Last Name </th>
                    <th>Email </th>
                    <th>DOB </th>
                    <th>Gender</th>
                    <th>Created At </th>

                </thead>
                <tbody>
                </tbody>
            </table>
        </div>

        @push('script')
        <script>
            $(document).ready(function() {
              $('#table').DataTable({
                ajax: "{{ route('admin.user.ajax')}}",
                columns: [
                            {data: 'DT_RowIndex', name: 'DT_RowIndex'},

                            {data: 'firstname', name: 'firstname'},
                            {data: 'lastname', name: 'lastname'},
                            {data: 'email', name: 'email'},
                            {data: 'date', name:'date'},
                            {data:"gender",name:'gender'},
                            {data:'create_at',name:'create_at'},

                        ]
              });
          });
           </script>
        @endpush
    </div>
@endsection
