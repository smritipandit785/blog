@extends('layout')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif

                    @foreach($users as $user)
                    <div class="card card-body m-2">
                        <p>Name :: {{$user->firstname}} {{$user->lastname}}</p>
                        <p>Email :: {{$user->email}}</p>
                        <p>Gender :: {{$user->gender}}</p>
                    </div>
                    @endforeach
                    @if($users->count() >0)
                    <p>No Match Find</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
