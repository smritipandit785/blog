<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(1000)->create();

        // for($i=0;$i<100;$i++)
        // {
        //     $profile = new User();
        //     $profile->name = $faker->name;
        //     $profile->email =$faker->unique()->email;
        //     $profile->password = Hash::make(12345678);
        //     $profile->save();
        //     $profile->profile()->create([
        //         "name" => $profile->name,
        //         "pincode" => $faker->postcode,
        //         "mobile" => $faker->e164PhoneNumber,
        //         "state" => $faker->state,
        //     ]);
        // }
    }
}
