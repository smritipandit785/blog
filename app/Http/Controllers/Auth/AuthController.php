<?php

namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use App\Models\Partner;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\Auth;
use Session;
use App\Models\User;
use Hash;

class AuthController extends Controller
{

    public function index(Request $request)
    {

        return view('auth.login');
    }

    public function registration()
    {
        return view('auth.registration');
    }

    public function postLogin(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);

        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            return redirect()->intended('dashboard')
                        ->withSuccess('You have Successfully loggedin');
        }

        return redirect("login")->withSuccess('Oppes! You have entered invalid credentials');
    }

    public function postRegistration(Request $request)
    {
        $request->validate([
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
            'date' => 'required',
            'income'=>'required',
            'gender'=> 'required',
            'occupation' => 'required',
            'familytype' => 'required',
            'manglik' => 'required',
            'expectedincome' => 'required',
            'partner_occupation'=>'required',
            'partner_familytype'=>'required',
            'partner_manglik'=>'required'
        ]);

        $data = $request->all();
        $check = $this->create($data);

        return redirect("dashboard")->withSuccess('Great! You have Successfully loggedin');
    }

    public function dashboard()
    {
        if(Auth::check()){

            $user = auth()->user();

            $users = User::where('amount','<',$user->partner->expectedincome)->where('manglik',$user->partner->partner_manglik)->where('occupation',$user->partner->partner_occupation)->where('familytype',$user->partner->partner_familytype)->where('id','!=',$user->id)->get();
            return view('dashboard',compact('users'));
        }

        return redirect("login")->withSuccess('Opps! You do not have access');
    }

    public function create(array $data)
    {
      $user = User::create([
                            'firstname' => $data['firstname'],
                            'lastname' =>$data['lastname'],
                            'email' => $data['email'],
                            'date' => $data['date'],
                            'password' => Hash::make($data['password']),
                            'gender'=>$data['gender'],
                            'amount' => $data['income'],
                            'occupation' => $data['occupation'],
                            'familytype' => $data['familytype'],
                            'manglik' => $data['manglik'],

                        ]);

        Partner::create([
                        'user_id'=>$user->id,
                        'expectedincome' => $data['expectedincome'],
                        'partner_occupation'=>$data['partner_occupation'],
                        'partner_familytype' => $data['partner_familytype'],
                        'partner_manglik' => $data['partner_manglik']
                     ]);

        return $user;

    }

    public function logout() {
        Session::flush();
        Auth::logout();

        return Redirect('login');
    }

}
