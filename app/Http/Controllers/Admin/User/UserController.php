<?php

namespace App\Http\Controllers\Admin\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use DataTables;
use Carbon\Carbon;

class UserController extends Controller
{
    public function getAjaxData(Request $request)
    {
            $data = User::select('*');



            return DataTables::eloquent($data)
                    ->addIndexColumn()
                    ->editColumn('familytype', function($row) {
                        $text = '';
                        switch($row->familytype)
                        {
                            case "jointfamily":
                                return "Joint Family";
                            case "nuclearfamily":
                                return "Nuclear Family";
                        }
                        return $text;
                    })

                    ->editColumn('occupation', function($row) {
                        $text = '';
                        switch($row->occupation)
                        {
                            case "privatejob":
                                return "Private Job";
                            case "governmentjob":
                                return "Government Job";
                            case "business":
                                return "Business";
                        }
                        return $text;
                    })

                    ->editColumn('date', function($row) {
                        $lastSeen = new Carbon($row->date);
                        if($lastSeen->isToday())
                            $lastSeen =$lastSeen->format(' ').' | Today';
                        else if($lastSeen->isYesterday())
                            $lastSeen =$lastSeen->format(' ').' | Yesterday';
                        else
                            $lastSeen = $lastSeen->format('M d Y');

                        return $lastSeen;
                    })
                    ->make(true);

    }

}
