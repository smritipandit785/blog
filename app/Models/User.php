<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Carbon\Carbon;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'firstname',
        'lastname',
        'email',
        'password',
        'date',
        'gender',
        'amount' ,
        'occupation' ,
        'familytype' ,
        'manglik'

    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends=['create_at'];

    public function getCreateAtAttribute()
    {

        $lastSeen = new Carbon($this->created_at);
        if($lastSeen->isToday())
            $lastSeen =$lastSeen->format(' h:i A').' | Today';
        else if($lastSeen->isYesterday())
            $lastSeen =$lastSeen->format(' h:i A').' | Yesterday';
        else
            $lastSeen = $lastSeen->format(' h:i A | M d');

        return $lastSeen;
    }

    public function partner()
    {
        return $this->hasOne('App\Models\Partner');
    }
}
