<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
    protected $fillable = ['user_id','expectedincome','partner_occupation','partner_familytype','partner_manglik'];
}
