<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Auth\AuthController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('admin')->name('admin.')->group(function(){
    Route::get('/index', function () {
        return view('admin.index');
    });

    Route::get('/user', function () {return view('admin.user');})->name('user');
    Route::get('user/ajax',"Admin\User\UserController@getAjaxData")->name('user.ajax');
});



Route::get('login', [AuthController::class, 'index'])->name('login');
Route::post('post-login', [AuthController::class, 'postLogin'])->name('login.post');
Route::get('registration', [AuthController::class, 'registration'])->name('register');
Route::post('post-registration', [AuthController::class, 'postRegistration'])->name('register.post');
Route::get('dashboard', [AuthController::class, 'dashboard']);
Route::get('logout', [AuthController::class, 'logout'])->name('logout');

/***Auth Route for Admin */
Route::get('admin/login','Admin\Auth\LoginController@showLoginForm')->name('admin.login');
Route::post('admin/login','Admin\Auth\LoginController@login')->name('admin.login');

Route::get('users', ['uses'=>'AuthController@index', 'as'=>'users.index']);

